-- Formatting

return {
  'stevearc/conform.nvim',
  opts = {
    formatters_by_ft = {
      python = {"ruff_format"},
      yaml = {"yamlfix"},
      terraform = {"terraform_fmt"},
      markdown = {"mdformat"},
      go = {"goimports", "gofmt"}
    }
  },
}
