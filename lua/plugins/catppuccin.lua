-- color theme

return {
  "catppuccin/nvim", 
  name = "catppuccin",
  priority = 1000, -- load first
  config = function() 
    -- load color theme
    vim.cmd.colorscheme "catppuccin-latte"
  end,
}