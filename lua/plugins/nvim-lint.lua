-- linting :D

return {
  'mfussenegger/nvim-lint',
  config = function (_,_)
    require("lint").linters_by_ft = {
      python = {"ruff"}
    }
  end,
}
