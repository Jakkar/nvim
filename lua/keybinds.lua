-- yoink https://github.com/soupglasses/dotfiles/blob/main/configs/nvim/.config/nvim/lua/keybinds.lua#L1
local function map_key(mode, lhs, rhs, opts)
  local options = { noremap = true, silent = true }
  if opts then
    options = vim.tbl_extend('force', options, opts)
  end
  vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end


-- replace with vim.keymap.set?

-- open/close neotree with leader + E
map_key('', '<Leader>E', ':Neotree toggle<CR>')
-- open/focus neotree with leader + e
map_key('', '<Leader>e', ':Neotree focus<CR>')

-- toggle vista with leader + v
map_key('', '<Leader>v', '<cmd>Vista!!<cr>')

-- toggle floating terminal
map_key('n', '<Leader>t', '<cmd>ToggleTerm direction=float<cr>')

-- lazygit floating terminal
map_key('n', '<Leader>g', '<cmd>lua _lazygit_toggle()<CR>')

-- Resize window using <ctrl> arrow keys
map_key("n", "<C-Up>", "<cmd>resize +2<cr>")
map_key("n", "<C-Down>", "<cmd>resize -2<cr>")
map_key("n", "<C-Left>", "<cmd>vertical resize -2<cr>")
map_key("n", "<C-Right>", "<cmd>vertical resize +2<cr>")

-- move between panes
map_key('n', '<C-h>', '<C-w>h')
map_key('n', '<C-j>', '<C-w>j')
map_key('n', '<C-k>', '<C-w>k')
map_key('n', '<C-l>', '<C-w>l')

-- allow terminal mode to be exited with ctrl + ESC
map_key('t', '<C-space>', '<C-\\><C-n>')

-- telescope
map_key("n", "<leader>ff", "<cmd>Telescope find_files<cr>")
map_key("n", "<leader>fg", "<cmd>Telescope live_grep<cr>")
map_key("n", "<leader>fb", "<cmd>Telescope current_buffer_fuzzy_find<cr>")

-- copy to system clipboard when prefixing with leader
map_key("", "<leader>y", '"+y')
